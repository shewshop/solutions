
# UVA 11341 - Term Strategy

| Time Limit | Data Size | Problem Type |
| --- | --- | --- |
| 1 second(s) | N/A | DP |

## Recurrence Relation

The cases for the reccurrence relation of this DP problem are roughly:

 * There are an impossible (possibly negative) amount of hours or courses
 * There is only one course and I should invest all possible hours into it
 * There are multiple courses, and if I invest `i` out of `m` hours into one,
 	then I have `n-i` hours to invest in the others, so I should get the max
	possible score from the different possible ways the `m` hours can be split

I've made a `foo` function that codfies these cases along with a `dp` double
array to store the results of `foo` for future use.

## Subproblems

There are two main subproblems in the given description:

 * Knowing when Peter doesn't have enough time
 * Knowing the max amount of score Peter can get

For the first, this can be determined by finding the total amount of hours
across all courses that is needed to get at least a 5 in each course (including
the 1 hour needed to even start studying for the course) and checking if the
cases' value for `M` is less than that. In my solution, I simply chose to have a
default negative value for any score less than passing and treat negative values
as a failed course. This is accounted for in the previous reccurrence relation
in that any time a negative value is found as a possible score, we ignore the
possibility of dedicating `n` hours into that course and continue normally.

For the second, simply solving the reccurrence relation described above for the
given `n` and `m` values will yield the right result.

## Notes and Gotcha's

One thing to note about this problem is that while it is asking for the best
average, the only variable value given the input is the possible score. This
means that to calculate the best average, it is sufficient to calculate the best
total score across all courses, and when displaying the value, dividing it by
the number of courses for the test case.

One gotcha is rounding; I saw some issues online with this problem because of
rounding. It seems sufficient to 'round up' your reported value by `1e-9` before
displaying it.

