#include <bits/stdc++.h>
using namespace std;

const int MAX_INT = std::numeric_limits<int>::max();
const int MIN_INT = std::numeric_limits<int>::min();
const int INF = 1.0e9;
const int NEG_INF = -1.0e9;

#define MEM(arr,val)memset(arr,val, sizeof arr)
#define PI acos(0)*2.0
#define eps 1.0e-9
#define are_equal(a,b)fabs(a-b)<eps
#define LS(b)(b& (-b)) // Least significant bit
#define DEG_to_RAD(a)((a*PI)/180.0) // convert to radians

#define forn(i,n) for(int (i) = 0; (i) < (n); (i)++)
#define fornm(i,n,m) for(int (i) = (n); (i) < (m); (i)++)
#define formn(i,m,n) for(int (i) = (m); (i) > (n); (i)--)
#define DEBUG if(debugging)

#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef pair<ii,int> iii;
typedef pair<int,char> ic;
typedef pair<long,char> lc;
typedef vector<int> vi;
typedef vector<ii> vii;

int gcd(int a,int b){return b == 0 ? a : gcd(b,a%b);}
int lcm(int a,int b){return a*(b/gcd(a,b));}
int cmp(const void *a, const void *b) {return *(int*)a - *(int*)b;}

const int debugging = 0;

const int T = 105, N = 15, M = 105;

int cases, courses, hours, res,
	l[N][M], dp[N][M];

int foo(int n, int m) {
	// Degenerate case
	if (n < 0 || m < 0) return MIN_INT;
	// Uncalculated case
	if (dp[n][m] != -1) return dp[n][m];
	// Base case; solve subproblem for a single course
	if (n == 1) return dp[n][m] = l[n][m];

	int s = MIN_INT;
	fornm(i,1,m) 
		if (l[n][i] >= 5)
			// Best score if we put i hours into course n and
			// the rest into the previous n-1 courses
			s = max(s, foo(n-1,m-i) + l[n][i]);

	return dp[n][m] = s;
}

int main() {
	FASTER;
	
	cin >> cases;
	while (cases--) {
		cin >> courses >> hours;
		forn(i,courses+1)
				MEM(dp[i],-1);

		forn(i,courses) {
			forn(j,hours) {
				cin >> l[i+1][j+1];
				if (l[i+1][j+1] < 5)
					// Simplifies our conditional logic in foo()
					l[i+1][j+1] = MIN_INT;
			}
		}

		res = foo(courses,hours);
		if (res < 0)
			printf("Peter, you shouldn't have played billiard that much.\n");
		else
			printf("Maximal possible average mark - %.2f.\n", (1.0*res/courses) + 1e-9);
	}
	
	return 0;
}
