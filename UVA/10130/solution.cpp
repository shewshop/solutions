#include <bits/stdc++.h>
using namespace std;

const int MAX_INT = std::numeric_limits<int>::max();
const int MIN_INT = std::numeric_limits<int>::min();
const int INF = 1.0e9;
const int NEG_INF = -1.0e9;

#define MEM(arr,val)memset(arr,val, sizeof arr)
#define PI acos(0)*2.0
#define eps 1.0e-9
#define are_equal(a,b)fabs(a-b)<eps
#define LS(b)(b& (-b)) // Least significant bit
#define DEG_to_RAD(a)((a*PI)/180.0) // convert to radians

#define forn(i,n) for(int (i) = 0; (i) < (n); (i)++)
#define fornm(i,n,m) for(int (i) = (n); (i) < (m); (i)++)
#define formn(i,m,n) for(int (i) = (m); (i) > (n); (i)--)
#define DEBUG if(debugging)

#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef pair<ii,int> iii;
typedef pair<int,char> ic;
typedef pair<long,char> lc;
typedef vector<int> vi;
typedef vector<ii> vii;

int gcd(int a,int b){return b == 0 ? a : gcd(b,a%b);}
int lcm(int a,int b){return a*(b/gcd(a,b));}
int cmp(const void *a, const void *b) {return *(int*)a - *(int*)b;}

const int debugging = 0;

const int T = 1005, N = 1005, P = 105, W = 35, G = 105, MW = 35;
int cases, n, g, wt, r,
	price[N], weight[N], maxWeight[G], KS[N][N*W+5];

int main() {
	FASTER;
	
	cin >> cases;
	while (cases--) {
		cin >> n;
		forn(i,n) {
			cin >> price[i+1] >> weight[i+1];
		}
		cin >> g;
		wt = 0;
		forn(i,g) {
			cin >> maxWeight[i+1];
			wt = max(wt, maxWeight[i+1]);
		}
		
		forn(i,n+1) {
			forn(w,wt+1) {
				if (i == 0 || w == 0) {
					KS[i][w] = 0;
					DEBUG printf("KS(%d,%d) set to %d\n", i, w, KS[i][w]);
				} else if (weight[i] <= w) {
					KS[i][w] = max(
						price[i] + KS[i-1][w-weight[i]],
						KS[i-1][w]
					);
					DEBUG printf("KS(%d,%d) set to %d\n", i, w, KS[i][w]);
				} else {
					KS[i][w] = KS[i-1][w];
					DEBUG printf("KS(%d,%d) set to %d\n", i, w, KS[i][w]);
				}
			}
		}

		r = 0;
		forn(i,g) {
			DEBUG printf("Adding KS(%d,%d)=%d\n", n, maxWeight[i+1], KS[n][maxWeight[i+1]]);
			r += KS[n][maxWeight[i+1]];
		}
		cout << r << endl;
	}
	
	return 0;
}
