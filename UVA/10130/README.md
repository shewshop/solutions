
# UVA 10130 - SuperSale

| Time Limit | Data Size | Problem Type |
| --- | --- | --- |
| 3 second(s) | N/A | DP |

## Recurrence Relation

Because for each group memeber the amount of each item is finite, the problem is
really an expansion on the classical
[0/1 Knapsack problem](http://www.geeksforgeeks.org/dynamic-programming-set-10-0-1-knapsack-problem/).

The difference is only that after the problem is solved for the largest weight
limit possible, the sum of all the given weight limits' solutions yields the
final answer.

In my solution, I've opted to implement this in a bottom-up approach instead of
memo-izing results of a recursive call.

## Subproblems

There are two parts to this problem:

 * Find the max value a single member can nab
 * Find the total value the group can nab

The first is solved by implementing 0/1 Knapsack for the largest weight. The
second is solved by simply summing up the results for the first problem.

## Notes and Gotcha's

Because I implemented the solution in a bottom-up approach, there is a question
of "up to what weight should 0/1 Knapsack be done?" The answer to this can be
seen to be the product of the largest weigt possible and the largest set size
possible for nabbable items.

